package org.bitbucket.rajibpsarma;

import java.util.List;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class DatabaseTest {
	public static void main(String[] args) {
		MongoClient client = new com.mongodb.MongoClient(new MongoClientURI("mongodb://localhost:27017"));
		List<String> dbNames = client.getDatabaseNames();
		System.out.println("List of databases : ");
		for(String dbName : dbNames) {
			System.out.println(dbName);
		}
	}
}
