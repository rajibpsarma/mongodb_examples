package org.bitbucket.rajibpsarma;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

/**
 * The following commands were used in MongoDB
 * 
 * use example
 * db.createCollection("cars")
 * 
 * var name = "Car #";
 * for(var index  = 1; index <= 5; index++) {
 *	db.cars.insert({
 *		srNo : index,
 *		id : name + index
 *	});
 * }
 * 
 * @author Rajib Sarma
 *
 */

public class QueryTest {
	public static void main(String[] args) {
		DB db = new ConnectionTest().getDatabase("example");
		DBCollection carColl = db.getCollection("cars");
		
		System.out.println("No of cars in the collection : " + carColl.count());
		
		
		// find all cars and display them
		// i.e. db.cars.find()
		DBObject obj = new BasicDBObject();
		DBCursor cursor = carColl.find();
		System.out.println("\n------------------------------------");
		System.out.println("Displaying the details of all the cars ...");
		displayCollectionDetails(cursor);
		
		// find the car with srNo 3
		// i.e. db.cars.find({srNo:3})
		obj = new BasicDBObject("srNo", 3);
		// Please note, the following will not fetch any result as srNo is number and not string
		// obj = new BasicDBObject("srNo", "3");
		cursor = carColl.find(obj);
		System.out.println("\n------------------------------------");
		System.out.println("Displaying the details of the car with srNo 3 ...");
		displayCollectionDetails(cursor);
		
		// find all cars and display them in descending order on srNo
		// i.e. db.cars.find().sort({srNo:-1})
		obj = new BasicDBObject();
		DBObject sortObj = new BasicDBObject("srNo", -1);
		cursor = carColl.find(obj).sort(sortObj);
		System.out.println("\n------------------------------------");
		System.out.println("Displaying the details of all the cars, in descending order of srNo ...");
		displayCollectionDetails(cursor);
		
		// find the car with highest srNo
		// i.e. db.cars.find().sort({srNo:-1}).limit(1)
		obj = new BasicDBObject();
		cursor = carColl.find(obj).sort(sortObj).limit(1);
		System.out.println("\n------------------------------------");
		System.out.println("Displaying the car with highest srNo ...");
		displayCollectionDetails(cursor);
		
		// find the cars with srNo > 3, i.e. {srNo:{$gt : 3}}
		obj = new BasicDBObject("srNo", new BasicDBObject("$gt", 3));
		cursor = carColl.find(obj);
		System.out.println("\n------------------------------------");
		System.out.println("Displaying the details of the cars with srNo > 3 ...");
		displayCollectionDetails(cursor);
		
		// AND condition
		// find based on name=�Arnab� AND id=2
		// i.e. db.students.find({$and:[{name:"Arnab"},{id:2}]})		
		DBCollection studentColl = db.getCollection("students");
		List<DBObject> list = new ArrayList<DBObject>();
		list.add(new BasicDBObject("name", "Arnab"));
		list.add(new BasicDBObject("id", 2));
		cursor = studentColl.find(new BasicDBObject("$and", list));
		System.out.println("\n------------------------------------");
		System.out.println("Displaying the details of the student with name=�Arnab� AND id=2 ...");
		displayCollectionDetails(cursor);
	}
	
	private static void queryUsingDatabase() {
		MongoDatabase db = ConnectionTest.getDB("example");
		MongoCollection<Document> carColl = db.getCollection("cars");
		Bson filter = new BasicDBObject("srNo", 2);
		FindIterable<Document> itr = carColl.find(filter);
		MongoCursor<Document> cursor = itr.cursor();
		Document doc = null;
		String key = null;
		while(cursor.hasNext()) {
			doc = cursor.next();
			//System.out.println(doc);
			Iterator<String> it = doc.keySet().iterator();
			while(it.hasNext()) {
				key = it.next();
				System.out.println(key + " = " + doc.get(key));
			}
		}
	}
	
	protected static void displayCollectionDetails(DBCursor cursor) {
		System.out.println("Total documents : "+ cursor.count());
		
		DBObject car = null;
		String key = null;
		Object val = null;
		int carNo = 1;
		while(cursor.hasNext()) {
			car = cursor.next();
			Iterator<String> keys = car.keySet().iterator();
			System.out.println("\nDetails of Document No : " + carNo++);
			while(keys.hasNext()) {
				key = keys.next();
				val = car.get(key);
				System.out.println(key + " = " + val);
			}
		}
	}
}
