package org.bitbucket.rajibpsarma;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

public class ConnectionTest {
	public static void main(String[] args) {
		DB db = new ConnectionTest().getDatabase("example");
		DBCollection col = db.getCollection("cars");
		System.out.println("No of documents in Cars collection :" + col.count());
	}
	
	public DB getDatabase(String dbName) {
		MongoClient client = new com.mongodb.MongoClient(new MongoClientURI("mongodb://localhost:27017"));
		return client.getDB(dbName);
//		MongoDatabase db = client.getDatabase(dbName);
//		return db;
	}
	
	public static MongoDatabase getDB(String dbName) {
		MongoClient client = new com.mongodb.MongoClient(new MongoClientURI("mongodb://localhost:27017"));
		return client.getDatabase(dbName);
	}
}
