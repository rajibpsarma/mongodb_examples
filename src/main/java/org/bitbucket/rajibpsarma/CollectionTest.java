package org.bitbucket.rajibpsarma;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * The following commands were used in MongoDB
 * 
 * use example
 * 
 * @author RSarma
 */
public class CollectionTest {
	public static void main(String[] args) {
		// Use db example
		DB db = new ConnectionTest().getDatabase("example");
		
		// Create customer collection
		final String collectionName =  "customers";
		DBCollection custColl = null;
		//custColl = db.createCollection(collectionName, new BasicDBObject());
		// Display the contents of the collection
		//System.out.println("\n--------------------- The empty collection ");
		//QueryTest.displayCollectionDetails(custColl.find());
		
		// Fetch a collection and add documents
		custColl = db.getCollection(collectionName);
		List<DBObject> documents = buildDocuments();
		custColl.insert(documents);
		// Display the contents of the collection
		System.out.println("\n--------------------- The initial collection with 3 documents");
		QueryTest.displayCollectionDetails(custColl.find());
		
		// update the document with name Rajib to Rajib P Sarma
		// i.e. db.<collection name>.update({"name":"Rajib Sarma"}, {$set:{"name":"Rajib P Sarma"}})
		DBObject query = new BasicDBObject("name", "Rajib Sarma");
		DBObject update = new BasicDBObject("$set", new BasicDBObject("name", "Rajib P Sarma"));
		custColl.update(query, update);
		// Display the contents of the collection
		System.out.println("\n--------------------- The collection after update");
		QueryTest.displayCollectionDetails(custColl.find());	
		
		// Find customers with names starting with "Ra" 
		System.out.println("\n--------------------- The customers starting with 'Ra'");
		QueryTest.displayCollectionDetails(custColl.find(new BasicDBObject("name", new BasicDBObject("$regex", "^Ra"))));	
		
		// remove a document
		custColl.remove(new BasicDBObject("name", "Chakri"));
		// Display the contents of the collection
		System.out.println("\n--------------------- The collection after removing Chakri");
		QueryTest.displayCollectionDetails(custColl.find());	
		
		// remove all document
		custColl.remove(new BasicDBObject());
		// Display the contents of the collection
		System.out.println("\n--------------------- The collection after removing all");
		QueryTest.displayCollectionDetails(custColl.find());	
	}
	
	private static List<DBObject> buildDocuments() {
		List<DBObject> documents = new ArrayList<DBObject>();
		
		DBObject cust1 = new BasicDBObject();
		cust1.put("name", "Rajib Sarma");
		cust1.put("address", "Gunjur, Bangalore");
		cust1.put("pin", 560087);
		documents.add(cust1);
		
		cust1 = new BasicDBObject();
		cust1.put("name", "Chakri");
		cust1.put("address", "Gunjur, Bangalore");
		cust1.put("pin", 560087);
		documents.add(cust1);
		
		cust1 = new BasicDBObject();
		cust1.put("name", "Ram Prasad");
		cust1.put("address", "Kundanhalli, Bangalore");
		cust1.put("pin", 560037);
		documents.add(cust1);
		
		return documents;
	}
}
